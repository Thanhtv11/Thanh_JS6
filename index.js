//   Bài 1

// Input : S , num
// Process: cứ sau mỗi vòng lặp thì tổng S sẽ được cộng dồn với n cho đến khi S > 10000 thì thoát khỏi vòng lặp
// Output : S và num mới
  var S = 0;
  var num = 0;

  function ketQua(){
    for ( var S = 0; S < 10000; num++){
        S = S + num;
        document.getElementById("demo").innerHTML = `👉Số nguyên dương nhỏ nhất: ${num}`
    }
  }
 

// Bài 2

// Input: nhapXValue , nhapNValue , soMu , tinhTong 
// Process: Vòng lặp và cộng dồn chuỗi
// Output: Tổng
function tinhTong(){
    var nhapXValue = document.getElementById("txt-nhap-x").value*1;
    var nhapNValue = document.getElementById("txt-nhap-n").value*1;
    var soMu = 1;
    var tinhTong = 0;

    for(var i = 1; i <= nhapNValue; i++){
       soMu = soMu * nhapXValue;
       tinhTong = tinhTong + soMu;
    }
    document.getElementById("demo2").innerHTML = `👉 Tổng: ${tinhTong}`;
}


// Bài 3
// Input :  S , number
// Process: Áp dụng vòng lặp  
// Output : Giai thừa


function nhanNut(){

var S = 1
var number = document.getElementById("txt-nhap-gt").value*1;

for (var i = 1; i <= number; i++){
    S = S * i;
    console.log(S);
}
document.getElementById("demo3").innerHTML =`👉 Giai Thừa: ${S}`

}

